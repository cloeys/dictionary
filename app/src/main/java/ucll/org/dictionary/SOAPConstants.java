package ucll.org.dictionary;

/**
 * Created by Christophe on 21/10/2016.
 */

public class SOAPConstants {

    public final static String SOAP_ACTION = "http://services.aonaware.com/webservices/";
    public final static String NAME_SPACE = "http://services.aonaware.com/webservices/";
    public final static String URL = "http://services.aonaware.com/DictService/DictService.asmx";
    public final static String DEFINE_METHOD_NAME = "Define";
    public final static String DEFINE_METHOD_ACTION = SOAP_ACTION + DEFINE_METHOD_NAME;
}
