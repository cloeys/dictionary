package ucll.org.dictionary;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class MainActivity extends AppCompatActivity {

    private EditText lookupText;
    private ListView definitionsList;
    private List<Definition> definitions = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lookupText = (EditText) findViewById(R.id.lookup_text);
        definitionsList = (ListView) findViewById(R.id.lookup_definitions);

    }

    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = prefs.edit();
        int max = 0;
        for (Definition d: definitions) {
            max++;
            edit.putString("DEFINITION_" + max, d.getDefinition());
            edit.putString("DICTIONARY_" + max, d.getDictionary());
        }
        edit.putInt("max", max);
        edit.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        int max = prefs.getInt("max", 0);
        for (int i = 1; i <= max; i++) {
            Definition d = new Definition();
            d.setDictionary(prefs.getString("DICTIONARY_" + i, "ERROR"));
            d.setDefinition(prefs.getString("DEFINITION_" + i, "ERROR"));
            definitions.add(d);
        }
        populateListView(definitions);
    }

    private String definitionsToString(List<Definition> definitions) {
        String res = "";
        for (Definition d: definitions) {
            res += "From " + d.getDictionary() + ": " + d.getDefinition() + "\n";
        }
        return res;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.choose_dictionary:
                startActivity(new Intent(this, DictionariesActivity.class));
                return true;
            case R.id.share:
                Intent send = new Intent(Intent.ACTION_SENDTO);
                String uriText = "mailto:" + Uri.encode("") +
                        "?subject=" + Uri.encode("Definitions for the word " + lookupText.getText().toString()) +
                        "&body=" + Uri.encode(definitionsToString(definitions));
                Uri uri = Uri.parse(uriText);

                send.setData(uri);
                startActivity(Intent.createChooser(send, "Send mail..."));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onLookupClicked(View view) {
        if (lookupText != null) {
            Log.d("Activity", lookupText.getText().toString());
            invokeAsyncTask("word", SOAPConstants.DEFINE_METHOD_ACTION, SOAPConstants.DEFINE_METHOD_NAME);
        }
    }

    private void invokeAsyncTask(String convertParams, String soapAction, String methodName) {
        new FetchDefinitionTask(this, soapAction, methodName, convertParams).execute(lookupText.getText()
                .toString().trim());
    }

    public void callBackDataFromAsyncTask(String result) {

        //Log.d("result", result);
        try {
            SAXParserFactory spf = SAXParserFactory.newInstance();
            SAXParser sp = spf.newSAXParser();
            XMLReader xr = sp.getXMLReader();

            /** Create handler to handle XML Tags ( extends DefaultHandler ) */
            DefinitionParser handler = new DefinitionParser();
            xr.setContentHandler(handler);
            InputStream is = new ByteArrayInputStream(result.getBytes());

            BufferedReader in = new BufferedReader(new InputStreamReader(is));

            String inputLine = "";
            String tmp;
            while ((tmp = in.readLine()) != null) {
                inputLine += tmp;
            }
            in.close();

            ByteArrayInputStream iss = new ByteArrayInputStream(inputLine.getBytes());

            xr.parse(new InputSource(iss));

            List<Definition> resultsList = handler.getDefinitionList();
            definitions = resultsList;

            populateListView(resultsList);
        } catch (Exception e) {
            Log.d("SAX", e.getMessage());
        }
    }

    public void populateListView(List<Definition> definitions) {
        //take list of definitions and put them in definitionsList
        DefinitionAdapter adapter = new DefinitionAdapter(this, definitions);
        definitionsList.setAdapter(adapter);
    }
}
