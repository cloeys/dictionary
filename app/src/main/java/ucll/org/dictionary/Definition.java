package ucll.org.dictionary;

/**
 * Created by Christophe on 21/10/2016.
 */

public class Definition {

    private String word;
    private String dictionary;
    private String definition;

    public Definition() {}

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDictionary() {
        return dictionary;
    }

    public void setDictionary(String dictionary) {
        this.dictionary = dictionary;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }
}
