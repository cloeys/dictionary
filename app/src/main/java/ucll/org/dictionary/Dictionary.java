package ucll.org.dictionary;

/**
 * Created by Christophe on 4/11/2016.
 */

public class Dictionary {

    private String name = "";
    private boolean checked = true;

    public Dictionary() {}
    public Dictionary(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void toggleChecked() {
        checked = !checked;
    }
}
