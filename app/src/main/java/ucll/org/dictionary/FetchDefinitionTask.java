package ucll.org.dictionary;

import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by Christophe on 21/10/2016.
 */

public class FetchDefinitionTask extends AsyncTask<String, Void, String> {

    private MainActivity activity;
    private String soapAction;
    private String methodName;
    private String paramsName;
    private SoapObject resultObject;

    public FetchDefinitionTask(MainActivity activity, String soapAction, String methodName,
                                  String paramsName) {
        this.activity = activity;
        this.methodName = methodName;
        this.soapAction = soapAction;
        this.paramsName = paramsName;
    }

    @Override
    protected String doInBackground(String... params) {
        //create a new soap request object
        SoapObject request = new SoapObject(SOAPConstants.NAME_SPACE, methodName);
        //add properties for soap object
        //Log.d("Task", params[0]);
        request.addProperty(paramsName, params[0]);

        //request to server and get Soap Primitive response
        String res = WebServiceCall.callWSThreadSoapPrimitive(SOAPConstants.URL, soapAction, request);
        return res;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result == null) {
            //Log.i("FetchDefinitionTask", "cannot get result");
        } else {
            //invoke call back method of Activity
            activity.callBackDataFromAsyncTask(result);
        }
    }


}
