package ucll.org.dictionary;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DictionariesActivity extends FragmentActivity {

    private Dictionary[] dictionaries ;
    private ArrayAdapter<Dictionary> listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dictionaries);

        ListView dictionaryListView = (ListView) findViewById(R.id.dictionaryListView);

        dictionaryListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Dictionary dictionary = listAdapter.getItem(position);
                dictionary.toggleChecked();
                DictionaryViewHolder viewHolder = (DictionaryViewHolder) view.getTag();
                viewHolder.getCheckBox().setChecked(dictionary.isChecked());
            }
        });

        dictionaries = (Dictionary[]) getLastCustomNonConfigurationInstance();
        if (dictionaries == null) {
            dictionaries = new Dictionary[]{
                    new Dictionary("devils"),
                    new Dictionary("easton"),
                    new Dictionary("elements"),
                    new Dictionary("foldoc"),
                    new Dictionary("gazetteer"),
                    new Dictionary("gcide"),
                    new Dictionary("hitchcock"),
                    new Dictionary("jargon"),
                    new Dictionary("vera"),
                    new Dictionary("wn"),
                    new Dictionary("world02"),
            };
        }
        ArrayList<Dictionary> dictionaryList = new ArrayList<>();
        dictionaryList.addAll(Arrays.asList(dictionaries));

        listAdapter = new DictionaryArrayAdapter(this, dictionaryList);
        dictionaryListView.setAdapter( listAdapter );
    }

    private static class DictionaryViewHolder {
        private CheckBox checkBox;
        private TextView textView;
        public DictionaryViewHolder() {}
        public DictionaryViewHolder(TextView textView, CheckBox checkBox) {
            this.textView = textView;
            this.checkBox = checkBox;
        }

        public TextView getTextView() {
            return textView;
        }

        public void setTextView(TextView textView) {
            this.textView = textView;
        }

        public CheckBox getCheckBox() {
            return checkBox;
        }

        public void setCheckBox(CheckBox checkBox) {
            this.checkBox = checkBox;
        }
    }

    private static class DictionaryArrayAdapter extends ArrayAdapter<Dictionary> {

        private LayoutInflater inflater;

        public DictionaryArrayAdapter(Context context, List<Dictionary> dictionaryList ) {
            super( context, R.layout.dictionary_ids_row, R.id.dictionary_id, dictionaryList );
            // Cache the LayoutInflate to avoid asking for a new one each time.
            inflater = LayoutInflater.from(context) ;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            // Dictionary to display
            Dictionary dictionary = this.getItem( position );

            // The child views in each row.
            CheckBox checkBox ;
            TextView textView ;

            // Create a new row view
            if ( convertView == null ) {
                convertView = inflater.inflate(R.layout.dictionary_ids_row, null);

                // Find the child views.
                textView = (TextView) convertView.findViewById( R.id.dictionary_id );
                checkBox = (CheckBox) convertView.findViewById( R.id.dictionary_checked );

                // Optimization: Tag the row with it's child views, so we don't have to
                // call findViewById() later when we reuse the row.
                convertView.setTag( new DictionaryViewHolder(textView,checkBox) );

                // If CheckBox is toggled, update the planet it is tagged with.
                checkBox.setOnClickListener( new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v ;
                        Dictionary dictionary = (Dictionary) cb.getTag();
                        dictionary.setChecked( cb.isChecked() );
                    }
                });
            }
            // Reuse existing row view
            else {
                // Because we use a ViewHolder, we avoid having to call findViewById().
                DictionaryViewHolder viewHolder = (DictionaryViewHolder) convertView.getTag();
                checkBox = viewHolder.getCheckBox() ;
                textView = viewHolder.getTextView() ;
            }

            // Tag the CheckBox with the Planet it is displaying, so that we can
            // access the planet in onClick() when the CheckBox is toggled.
            checkBox.setTag( dictionary );

            // Display planet data
            checkBox.setChecked( dictionary.isChecked() );
            textView.setText( dictionary.getName() );

            return convertView;
        }

    }

    public Object onRetainCustomNonConfigurationInstance () {
        return dictionaries ;
    }

}
