package ucll.org.dictionary;

import android.util.Log;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christophe on 21/10/2016.
 */

public class DefinitionParser extends DefaultHandler {

    private List<Definition> definitionList;
    private String val;
    private Definition curr;

    public DefinitionParser() {
        definitionList = new ArrayList<>();
        val = "";
        curr = new Definition();
    }

    @Override
    public void characters(char[] buffer, int start, int length) {
        val = val + new String(buffer, start, length);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        val = "";
        if (qName.equalsIgnoreCase("definition")) {
            curr = new Definition();
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("word")) {
            curr.setWord(val);
        } else if (qName.equalsIgnoreCase("id")) {
            curr.setDictionary(val);
        } else if (qName.equalsIgnoreCase("worddefinition")) {
            curr.setDefinition(val);
        } else if (qName.equalsIgnoreCase("definition")) {
            definitionList.add(curr);
        }
    }

    public List<Definition> getDefinitionList() {
        return definitionList;
    }
}
