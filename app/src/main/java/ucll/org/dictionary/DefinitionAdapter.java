package ucll.org.dictionary;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Christophe on 21/10/2016.
 */

public class DefinitionAdapter extends ArrayAdapter<Definition> {

    public DefinitionAdapter(Context context, List<Definition> definitions) {
        super(context, 0, definitions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Definition definition = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dictionary_list, parent, false);
        }
        // Lookup view for data population
        TextView dDictionary = (TextView) convertView.findViewById(R.id.dDictionary);
        TextView dDefinition = (TextView) convertView.findViewById(R.id.dDefinition);
        // Populate the data into the template view using the data object
        dDictionary.setText(definition.getDictionary());
        dDefinition.setText(definition.getDefinition());
        // Return the completed view to render on screen
        return convertView;
    }

}

